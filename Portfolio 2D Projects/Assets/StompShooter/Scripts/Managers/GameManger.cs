﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameManger : MonoBehaviour {
	private static GameManger instance ;

	GameState _currentGameState = GameState.PREGAME;
	public enum GameState
	{
		PREGAME,
		RUNNING,
		PAUSED,
		GAMEOVER
	}


	public GameState CurrentGameState
	{
		get
		{
			return _currentGameState;
		}
		private set
		{
			_currentGameState = value;
		}
	}


	public static GameManger Instance
	{
		get{ return instance;}
	}
	public int score = 0;
	public int health = 5;
	public bool isStomping;
	public event Action<GameState,GameState> OnGameStateChanged;
	public event Action<int> onScoreUpdate;
	public event Action OnGameStart;

	public List<GameObject> attackableObjects = new List<GameObject>();
	public int HighScore
    {
		get { return highscore; }
    }

	AudioSource audioSource;
	int highscore;
	void Awake()
	{
		if(instance == null)
			instance = this;
	}
	// Use this for initialization
	void Start () 
	{
		attackableObjects.Clear();
		UpdateState(GameState.PREGAME);		
		audioSource = GetComponent<AudioSource>();
		if (PlayerPrefs.HasKey("HighScore"))
			highscore = PlayerPrefs.GetInt("HighScore");
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (_currentGameState == GameState.PREGAME)
		{
			return;
		}

		if (Input.GetKeyDown(KeyCode.Escape))
		{
			if (CurrentGameState == GameState.GAMEOVER || CurrentGameState == GameState.PREGAME)
				return;
			TogglePause();
		}

	}

	public void SaveHighScore()
    {
		if(PlayerPrefs.HasKey("HighScore"))
        {			
			if(score > highscore)
            {
				PlayerPrefs.SetInt("HighScore", score);
				Debug.Log("Setting Highscore");
				highscore = PlayerPrefs.GetInt("HighScore", score);
			}
        }
		else
        {
			PlayerPrefs.SetInt("HighScore", score);
			highscore = PlayerPrefs.GetInt("HighScore", score);
        }
    }


	public void AddObjects(GameObject _object)
    {
		attackableObjects.Add(_object);
    }

	public void RemoveObject(GameObject _object)
    {
		attackableObjects.Remove(_object);
	}


	public void PlaySFX(AudioClip clip,AudioSource _audioSource=null)
    {

		if (_audioSource == null)
			_audioSource = audioSource;
		_audioSource.Stop();

		_audioSource.clip = clip;
		_audioSource.Play();
    }


	public void AddScore(int _score)
    {
		score += _score;
		if (onScoreUpdate != null)
		{
			onScoreUpdate(score);
		}
	}

	public void StartGame()
    {
		UpdateState(GameState.RUNNING);
		if(OnGameStart != null)
        {
			OnGameStart();
        }
    }

	public void EndGame()
    {
		UpdateState(GameState.GAMEOVER);
    }


	public void TogglePause()
	{
		Debug.Log("Toggle Pause");
		UpdateState(_currentGameState == GameState.RUNNING ? GameState.PAUSED : GameState.RUNNING);
	}

	void UpdateState(GameState state)
	{
		GameState previousGameState = _currentGameState;

		_currentGameState = state;

		switch (_currentGameState)
		{
			case GameState.PREGAME:
				Time.timeScale = 0;
				break;
			case GameState.RUNNING:
				Time.timeScale = 1;
				break;
			case GameState.PAUSED:
				Time.timeScale = 0;
				break;
			case GameState.GAMEOVER:
				Time.timeScale = 0;
				break;
			default:
				break;
		}
		if(OnGameStateChanged != null)
        {
			OnGameStateChanged(_currentGameState, previousGameState);
        }

	}

}
