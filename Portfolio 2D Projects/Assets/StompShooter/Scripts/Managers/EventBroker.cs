﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using UnityEngine;

public class EventBroker
{
    public static event Action<int> OnTakeDamage;
    public static event Action<GameObject> OnEnemyDeath;
    public static event Action<Ability> OnAbilityPickUp;

    #region HealthEventCalls
   
    public static void CallOnTakeDamage(int damage)
    {
        if (OnTakeDamage != null)
            OnTakeDamage(damage);
    }


    #endregion
    public static void CallOnEnemyDeath(GameObject enemy)
    {
        if (OnEnemyDeath != null)
            OnEnemyDeath(enemy);
    }

    public static void CallOnAbilityPickup(Ability ability)
    {
        if (OnAbilityPickUp != null)
            OnAbilityPickUp(ability);
    }






}
