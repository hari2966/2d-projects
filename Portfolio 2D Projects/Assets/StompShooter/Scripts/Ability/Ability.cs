﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Ability : ScriptableObject
{
    public Sprite aIcon;
    public float CoolDown;
    public virtual void InitializeAbility(GameObject target)
    {
        EventBroker.CallOnAbilityPickup(this);
    }
    
}
