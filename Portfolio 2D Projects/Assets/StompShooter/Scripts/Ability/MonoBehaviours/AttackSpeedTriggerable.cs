﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackSpeedTriggerable : MonoBehaviour
{
    [HideInInspector]public float Cooldown;
    bool isActive;
    private float prevAttackSpeed;
    Player player;
    
    public void ActivateAbility(GameObject target)
    {
        if(isActive)
        {
            StopAllCoroutines();
            StartCoroutine(ResetAttackSpeed(target, Cooldown));
        }
        else
        {
            isActive = true;
            player = target.GetComponent<Player>();
            prevAttackSpeed = player.attackRate;
            player.attackRate = 0.07f;
            StartCoroutine(ResetAttackSpeed(target, Cooldown));
        }
       
    }

    IEnumerator ResetAttackSpeed(GameObject target,float coolDown)
    {
        yield return new WaitForSeconds(coolDown);
        isActive = false;
        player.attackRate = prevAttackSpeed;
    }
}
