﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldTrigger : MonoBehaviour
{
    [HideInInspector]public float Cooldown;

   public void ActivateAbility(GameObject target)
    {
        GameObject shield = target.transform.Find("Shield").gameObject;
        shield.SetActive(true);
        StartCoroutine(DeactivateShield(shield, Cooldown));
    }


    IEnumerator DeactivateShield(GameObject shield,float CoolDown)
    {
        yield return new WaitForSeconds(CoolDown);
        shield.SetActive(false);
    }




}
