﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAllTriggerable : MonoBehaviour
{    
    public void ActivateAbility()
    {
        List<GameObject> enemyList = new List<GameObject>(GameManger.Instance.attackableObjects);
        GameManger.Instance.attackableObjects.Clear();
        foreach (GameObject enemy in enemyList)
        {
            if (enemy == null)
                return;
            IAttackable attack = enemy.GetComponent<IAttackable>();

            if(attack != null)
            {
                attack.OnAttack(100);
            }
           
        }
        enemyList.Clear();
       


    }
}
