﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageBoostTriggerable : MonoBehaviour,ITriggerable
{
    [HideInInspector]public float Cooldown;
    [HideInInspector] public int bonusDamage;
    Player _player;
    int prevDamage;
    public void ActivateAbility(GameObject target)
    {
        _player = target.GetComponent<Player>();
        prevDamage = _player.damage;
        _player.damage += bonusDamage;
        StartCoroutine(resetAbility(Cooldown));
    }

    public IEnumerator resetAbility(float _coolDown)
    {
        yield return new WaitForSeconds(_coolDown);
        _player.damage = prevDamage;
    }   
}
