﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TripleAttackTriggerable : MonoBehaviour,ITriggerable
{
    [HideInInspector] public float Cooldown;
    Transform dualShot;
    Player _player;
    public void ActivateAbility(GameObject player)
    {
        _player = player.GetComponent<Player>();

        Transform tripleShot = player.transform.Find("TripleShot");
        dualShot = player.transform.Find("DualShot");
        _player.spawnParent = tripleShot;
        StartCoroutine(resetAbility(Cooldown));
    }

    public IEnumerator resetAbility(float _coolDown)
    {
        yield return new WaitForSeconds(_coolDown);
        _player.spawnParent = dualShot;
    }
}
