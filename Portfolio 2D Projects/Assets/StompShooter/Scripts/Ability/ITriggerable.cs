﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITriggerable 
{
    void ActivateAbility(GameObject target);
    IEnumerator resetAbility(float _coolDown);
}
