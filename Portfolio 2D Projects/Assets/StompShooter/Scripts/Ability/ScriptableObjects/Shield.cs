﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName ="Ability/Shield")]
public class Shield : Ability
{
    private ShieldTrigger sTrigger;
    public override void InitializeAbility(GameObject target)
    {
        base.InitializeAbility(target);
        if (target.GetComponent<ShieldTrigger>() == null)
            sTrigger = target.AddComponent<ShieldTrigger>();
        sTrigger.Cooldown = CoolDown;
        sTrigger.ActivateAbility(target);
    }
}
