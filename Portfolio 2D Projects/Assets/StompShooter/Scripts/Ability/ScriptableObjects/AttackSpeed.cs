﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Ability/AttackSpeed")]
public class AttackSpeed : Ability
{
    private AttackSpeedTriggerable aSTrigger;

    public override void InitializeAbility(GameObject target)
    {
        base.InitializeAbility(target);

        if (target.GetComponent<AttackSpeedTriggerable>() == null)
            aSTrigger = target.AddComponent<AttackSpeedTriggerable>();
        aSTrigger.Cooldown = CoolDown;
        aSTrigger.ActivateAbility(target);
    }
}
