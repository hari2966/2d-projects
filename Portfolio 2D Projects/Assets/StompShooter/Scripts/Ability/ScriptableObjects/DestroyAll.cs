﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Ability/DestroyAll")]
public class DestroyAll : Ability
{
    private DestroyAllTriggerable dTrigger;
    public override void InitializeAbility(GameObject target)
    {
        if (target.GetComponent<DestroyAllTriggerable>() == null)
            dTrigger = target.AddComponent<DestroyAllTriggerable>();
        dTrigger.ActivateAbility();
    }
}
