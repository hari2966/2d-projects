﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu (menuName ="Ability/TripleAttack")]
public class TripleAttack : Ability
{
    private TripleAttackTriggerable trTrigger;
    public override void InitializeAbility(GameObject target)
    {
        base.InitializeAbility(target);
        if (target.GetComponent<TripleAttackTriggerable>() == null)
            trTrigger = target.AddComponent<TripleAttackTriggerable>();
        trTrigger.Cooldown = CoolDown;
        trTrigger.ActivateAbility(target);
    }
}
