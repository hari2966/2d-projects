﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Ability/DamageBoost")]
public class DamageBoost : Ability
{
    DamageBoostTriggerable dbTrigger;
    public int bonusDamage;

    public override void InitializeAbility(GameObject target)
    {
        base.InitializeAbility(target);

        if (target.GetComponent<DamageBoostTriggerable>() == null)
            dbTrigger = target.AddComponent<DamageBoostTriggerable>();
        dbTrigger.Cooldown = CoolDown;
        dbTrigger.bonusDamage = bonusDamage;
        dbTrigger.ActivateAbility(target);
    }
}
