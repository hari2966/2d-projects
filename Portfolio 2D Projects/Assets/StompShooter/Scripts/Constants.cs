﻿using UnityEngine;
using System.Collections;

public class Constants : MonoBehaviour {

	//public const string INPUT_HORIZONTAL = "Horizontal";
	//public const string INPUT_VERTICAL = "Vertical";
	//public const string TAG_ENEMY = "Enemy";
	public const string TAG_PLAYER = "Player";
	public const string TAG_PLAYER_BULLET = "PlayerBullet";
	public const string TAG_ENEMY_BULLET = "EnemyBullet";
	public const string TAG_BOUNDARY = "Boundary";

	public const string SAVED_VAL_VOLUME = "Voulme";
	public const string SAVED_VAL_AUDIO_TOGGLE = "AudioToggle";
	public const string SAVED_VAL_BEST_SCORE = "Score";



}
