﻿using UnityEngine;
using System;
using System.Collections;

public class BulletMovement : MonoBehaviour {
	public float speed;
	public Rigidbody2D myrigid;
	public static int playerDamage = 5;
	public static int playercriticalDamage = 100;
	public int bulletDamage;



	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
		myrigid.velocity =  transform.up * speed;
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "Boundary") 
		{
			Destroy (gameObject);
		}

		IAttackable attackObject = other.gameObject.GetComponent<IAttackable>();


		if (attackObject != null)
		{
			attackObject.OnAttack(bulletDamage);
			Destroy(gameObject);
		}

		//if (other.gameObject.tag == "Enemy")
  //      {
		//	if(other.gameObject.GetComponent<Enemy>())
  //          {
		//		Enemy enemy = other.gameObject.GetComponent<Enemy>();
		//		enemy.CallOnTakeDamage(bulletDamage);
		//		Destroy(gameObject);
		//	}

			

		//}
		

	}

}
