﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Player : MonoBehaviour
{


	public float speed;
	public float attackRate;
	public int maxHealth;
	public Camera MainCamera;
	public GameObject bullet;
	public Transform spawnParent;
	public int damage;

	public AudioClip playerLaserSound;
	public AudioClip playerImpactSound;
	public GameObject hurtParticleEffect;
	int currentHealth;
	float nextInterval = 0.0f;
	private Vector2 screenBounds;
	private float objectWidth;
	private float objectHeight;
	GameObject enemy;
	private float m_MovementSmoothing = .05f;
	AudioSource aSource;
	float hAxis;
	float vAxis;
	Animator anim;

	Rigidbody2D myRigid;
	Vector3 velocity;

	public int CurrentHealth
	{
		get { return currentHealth; }
	}

	

	// Use this for initialization
	void Start () {
		enemy = GameObject.FindGameObjectWithTag ("Enemy");
		aSource = GetComponent<AudioSource>();
		myRigid = GetComponent<Rigidbody2D>();
		anim = GetComponent<Animator>();
		currentHealth = maxHealth;
		screenBounds = MainCamera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, MainCamera.transform.position.z));
		objectWidth = transform.GetComponent<SpriteRenderer>().bounds.extents.x; //extents = size of width / 2
		objectHeight = transform.GetComponent<SpriteRenderer>().bounds.extents.y; //extents = size of height / 2
	}

    private void OnEnable()
    {
        EventBroker.OnTakeDamage += Player_OnTakeDamage;   
    }
	private void OnDisable()
	{
		EventBroker.OnTakeDamage -= Player_OnTakeDamage;
	}

	private void Player_OnTakeDamage(int obj)
    {
		GameManger.Instance.PlaySFX(playerImpactSound,aSource);
		GameObject hurtPartClone = Instantiate(hurtParticleEffect, transform.position, Quaternion.identity) as GameObject;
		Destroy(hurtPartClone, 1f);
		anim.SetTrigger("Hurt");
		currentHealth -= obj;

		if(currentHealth <= 0)
        {
			Destroy(gameObject);
			GameManger.Instance.EndGame();
        }
	}

    // Update is called once per frame
    void Update () 
	{
		if (GameManger.Instance.CurrentGameState != GameManger.GameState.RUNNING)
			return;
		hAxis = Input.GetAxisRaw("Horizontal") * speed;
		vAxis = Input.GetAxisRaw("Vertical") * speed;

		if (Input.GetMouseButton(0))
		{
			//Shoot Code Here

			if (Time.time > nextInterval)
			{
                foreach (Transform child in spawnParent)
                {
					GameObject bulletClone = Instantiate(bullet, child.position, child.rotation) as GameObject;
					BulletMovement bulletMov = bulletClone.GetComponent<BulletMovement>();
					bulletMov.bulletDamage = damage;
				}				
				nextInterval = Time.time + attackRate;
				GameManger.Instance.PlaySFX(playerLaserSound);

			}
		}

	}
	void LateUpdate()
	{
		Vector3 viewPos = transform.position;
		viewPos.x = Mathf.Clamp(viewPos.x, screenBounds.x * -1 + objectWidth, screenBounds.x - objectWidth);
		viewPos.y = Mathf.Clamp(viewPos.y, screenBounds.y * -1 + objectHeight, screenBounds.y - objectHeight);
		transform.position = viewPos;
	}

	void FixedUpdate()
	{

		Vector3 targetVelocity = new Vector2(hAxis * Time.fixedDeltaTime, vAxis * Time.fixedDeltaTime);
		// And then smoothing it out and applying it to the character
		myRigid.velocity = Vector3.SmoothDamp(myRigid.velocity, targetVelocity, ref velocity, m_MovementSmoothing);

			
	}		



}
