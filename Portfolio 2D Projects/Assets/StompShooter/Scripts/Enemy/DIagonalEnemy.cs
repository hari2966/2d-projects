﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DIagonalEnemy : MonoBehaviour,IAttackable
{
    public float speed;
    public int enemyHealth = 100;
    public GameObject impactParticle;
    public GameObject explosionParticle;
    Rigidbody2D enemyBody;
    Animator anim;
    bool isDead;


    private void Start()
    {
        anim = GetComponent<Animator>();
        enemyBody = GetComponent<Rigidbody2D>();
    }
    private void Update()
    {
        enemyBody.velocity = new Vector3(-transform.right.x * speed * Time.deltaTime, transform.up.y * speed * Time.deltaTime, 0);
    }


   

    void HandleDeath()
    {
        if (isDead)
            return;
        EventBroker.CallOnEnemyDeath(gameObject);
        GameObject particleClone = Instantiate(explosionParticle, transform.position, Quaternion.identity) as GameObject;
        particleClone.transform.localScale /= 2;
        Destroy(particleClone, 1f);
        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.CompareTag("Player"))
        {
            EventBroker.CallOnTakeDamage(10);
            HandleDeath();
        }
    }

    public void OnAttack(int damage)
    {

        anim.SetTrigger("Hurt");
        GameObject particleClone = Instantiate(impactParticle, transform.position, Quaternion.identity) as GameObject;
        particleClone.transform.localScale /= 2;
        Destroy(particleClone, 1f);
        enemyHealth -= damage;
        if (enemyHealth <= 0)
        {
            HandleDeath();
            isDead = true;

        }
    }

    


}
