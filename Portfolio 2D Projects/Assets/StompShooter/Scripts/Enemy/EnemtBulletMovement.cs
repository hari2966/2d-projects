﻿using UnityEngine;
using System.Collections;

public class EnemtBulletMovement : MonoBehaviour,IAttackable 
{
	public float speed;
	public Rigidbody2D myrigid;
	public int bulletDamage;
	public AnimationClip impactClip;
	Animator anim;
	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator>();
	}

	// Update is called once per frame
	void Update () {

		myrigid.velocity =  Vector2.left * speed;

	}


	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "Player") 
		{
			EventBroker.CallOnTakeDamage(bulletDamage);
			HandleDeath();
			
		}


	}



	void HandleDeath()
    {
		EventBroker.CallOnEnemyDeath(gameObject);
		speed = 0;
		anim.SetTrigger("Impact");
		gameObject.GetComponent<BoxCollider2D>().enabled = false;
		Destroy(gameObject,0.2f);
	}

    public void OnAttack(int damage)
    {
		HandleDeath();
	}
}
