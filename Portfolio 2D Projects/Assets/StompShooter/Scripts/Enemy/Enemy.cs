﻿using UnityEngine;
using System;
using UnityEngine.Events;
using System.Collections;

public class Enemy : MonoBehaviour,IAttackable
{

	//Public variables
	public float speed;
	public GameObject bullet;
	public Rigidbody2D EnemyBody;
	public float fireIntervl = 0.1f;
	public Transform shotSpawn;
	public  int enemyHealth = 100;
	public AudioClip enemyBulletSound;
	public AudioClip enemyDamageSound;
	public AudioClip enemyExplosionSound;
	AudioSource enemyAudio;
	public int enemyPoints;
	public GameObject enemyParticle;
	//Private variables
	float extremeLeft, extremeRight, extremeUp, extremeDown;
	float nextInterval = 0.0f;
	Animator anim;
	bool isDead;


	// Use this for initialization
	void Start () 
	{
		anim = GetComponent<Animator>();
		enemyAudio = GetComponent<AudioSource>();
	}

    private void OnEnable()
    {
    }

	private void OnDisable()
	{
	}
		

    void Update()
	{
		//Debug.Log ("Enemy Health = " + enemyHealth);
		if (Time.time > nextInterval) 
		{
			
			GameManger.Instance.PlaySFX(enemyBulletSound);
			nextInterval = Time.time+fireIntervl;
			GameObject bulletClone = Instantiate (bullet, transform.position, transform.rotation) as GameObject;
			GameManger.Instance.AddObjects(bulletClone);
		}
		EnemyBody.velocity =  Vector2.left * speed;
	}
	

	void PlaySFX(AudioClip clip)
    {		
		enemyAudio.Stop();

		enemyAudio.clip = clip;
		enemyAudio.Play();
	}

	void HandleDeath()
    {
		if (isDead)
			return;
		PlaySFX(enemyExplosionSound);
		gameObject.GetComponent<SpriteRenderer>().enabled = false;
		gameObject.GetComponent<BoxCollider2D>().enabled = false;
		GameObject particleClone = Instantiate(enemyParticle, transform.position, Quaternion.identity) as GameObject;
		GameManger.Instance.RemoveObject(gameObject);
		GameManger.Instance.AddScore(enemyPoints);
		Destroy(gameObject, 0.5f);
	}


	void OnTriggerEnter2D(Collider2D other)
	{
			
		if(other.gameObject.CompareTag("Player"))
        {
			EventBroker.CallOnTakeDamage(10);
			HandleDeath();
        }

	}

    public void OnAttack(int damage)
    {
		//Play animation here
		anim.SetTrigger("Hurt");
		PlaySFX(enemyDamageSound);
		enemyHealth -= damage;
		if (enemyHealth <= 0)
		{
			HandleDeath();
			isDead = true;

		}
	}
}
