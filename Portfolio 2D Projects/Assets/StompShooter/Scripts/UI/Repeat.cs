﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Repeat : MonoBehaviour {

	BoxCollider2D groundCollider;
	float horizontalLength;
	void Awake()
	{
		groundCollider = GetComponent<BoxCollider2D> ();
		horizontalLength = groundCollider.size.x;

	}


	
	// Update is called once per frame
	void Update () {

		if (transform.position.x < -horizontalLength) 
		{
			RepositionBackground ();
		}
	}

	void RepositionBackground ()
	{
		Vector2 groundOffset = new Vector2 (horizontalLength * 1.96f, 0);
		transform.position = (Vector2)transform.position + groundOffset;
	}

}
