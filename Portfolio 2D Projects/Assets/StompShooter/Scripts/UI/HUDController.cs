﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


public class HUDController : MonoBehaviour
{
    public RectTransform inGameUI;
    public Slider healthSlider;
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI healthText;
    private float prevFillAmount;
    public Player player;
    public GameObject[] abilityHolders;
    Ability prevAbility;  
    private void Start()
    {
        GameManger.Instance.onScoreUpdate += Instance_onScoreUpdate;
        EventBroker.OnTakeDamage += EventBroker_OnTakeDamage;
        EventBroker.OnAbilityPickUp += EventBroker_OnAbilityPickUp;
    }

    

    private void OnDisable()
    {
        GameManger.Instance.onScoreUpdate -= Instance_onScoreUpdate;
        EventBroker.OnTakeDamage -= EventBroker_OnTakeDamage;
        EventBroker.OnAbilityPickUp -= EventBroker_OnAbilityPickUp;
    }

    private void EventBroker_OnTakeDamage(int obj)
    {
        StartCoroutine(BarFade(healthSlider));
        SetSlider(healthSlider, player.CurrentHealth, healthText, player.maxHealth);
    }

    private void Instance_onScoreUpdate(int obj)
    {
        SetScore(obj);
    }


    private void EventBroker_OnAbilityPickUp(Ability obj)
    {
        int counter=0;
        GameObject abilityHolderObject = abilityHolders[counter];
        abilityHolderObject.SetActive(true);
        AbilityUI abilityHolder = abilityHolderObject.GetComponent<AbilityUI>();
        if(abilityHolder.isActive && prevAbility != obj)
        {
            counter++;
            abilityHolderObject = abilityHolders[counter];
            abilityHolderObject.SetActive(true);
            abilityHolder = abilityHolderObject.GetComponent<AbilityUI>();

        }
        prevAbility = obj;
        abilityHolder.ActivateAbility(obj);        
    }

    #region SliderFunctions
    private void InitializeBar(Slider bar, string barType)
    {
        Color tempColor;
        switch (barType)
        {
            case "Health":
                bar.maxValue = player.CurrentHealth;
                SetSlider(bar, player.CurrentHealth, healthText, player.maxHealth);
                break;           
            default:
                break;
        }
        Image frontFill = bar.transform.Find("FrontFill").GetComponent<Image>();
        Image backFill = bar.transform.Find("BackFill").GetComponent<Image>();
        bar.value = bar.maxValue;
        tempColor = backFill.color;
        tempColor.a = 0;
        backFill.color = tempColor;
        prevFillAmount = frontFill.fillAmount;
    }

    private void SetSlider(Slider slider, int currentVal, TextMeshProUGUI sliderText, int maxVal)
    {
        slider.maxValue = maxVal;
        slider.value = currentVal;
        sliderText.text = currentVal + "/" + maxVal;
    }
    

  
    public void SetScore(int _score)
    {
        scoreText.text = _score.ToString();
    }



    private IEnumerator BarFade(Slider bar)
    {
        Color tempColor;
        Image frontFill = bar.transform.Find("FrontFill").GetComponent<Image>();
        Image backFill = bar.transform.Find("BackFill").GetComponent<Image>();


        if (backFill.color.a <= 0)
        {
            backFill.fillAmount = frontFill.fillAmount;
        }
        tempColor = backFill.color;
        tempColor.a = 1;
        backFill.color = tempColor;
        yield return new WaitForSeconds(1f);
        tempColor.a = 0;
        backFill.color = tempColor;
    }


    #endregion



}
