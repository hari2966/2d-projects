﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AbilityUI : MonoBehaviour
{
    public Image abilitySprite;
    public Image backFill;
    private float abilityCooldown;
    private float nextReadyTime;
    private float coolDownTimeLeft;
    public bool isActive;
    public void ActivateAbility(Ability ability)
    {
        abilitySprite.sprite = ability.aIcon;
        abilityCooldown = ability.CoolDown;
        nextReadyTime = abilityCooldown + Time.time;
        coolDownTimeLeft = abilityCooldown;
        isActive = true;
    }

    private void Update()
    {
        if(isActive)
        {
            bool coolDownComplete = (Time.time > nextReadyTime);
            if (coolDownComplete)
            {
                AbilityComplete();
            }
            else
            {
                CoolDown();
            }
        }
        
    }


    private void AbilityComplete()
    {
        gameObject.SetActive(false);
        isActive = false;
    }



    private void CoolDown()
    {
        coolDownTimeLeft -= Time.deltaTime;
        backFill.fillAmount = (coolDownTimeLeft / abilityCooldown);

    }

}
