﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class UIManager : MonoBehaviour
{
    [SerializeField] private PauseMenu _pauseMenu;
    [SerializeField] private GameObject mainMenu;
    [SerializeField] private GameObject _gameOverMenu;

    public event Action OnGameOver;

    private void Start()
    {
        GameManger.Instance.OnGameStateChanged += Instance_OnGameStateChanged;
    }

    private void OnDisable()
    {
        GameManger.Instance.OnGameStateChanged -= Instance_OnGameStateChanged;
    }

    private void Instance_OnGameStateChanged(GameManger.GameState arg1, GameManger.GameState arg2)
    {
        if(arg1 == GameManger.GameState.GAMEOVER)
        {
            Debug.Log("zzz" + OnGameOver);
            _gameOverMenu.SetActive(true);
            if (OnGameOver != null)
                OnGameOver();
        }
        _pauseMenu.gameObject.SetActive(arg1 == GameManger.GameState.PAUSED);

    }

    private void Update()
    {
        if(GameManger.Instance.CurrentGameState == GameManger.GameState.PREGAME)
        {
            if(Input.GetKeyDown(KeyCode.Space))
            {
                mainMenu.SetActive(false);
                GameManger.Instance.StartGame();
            }
        }
    }
}
