﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scrolling : MonoBehaviour {

	Rigidbody2D scrollObjectRigid;
	// Use this for initialization
	void Start () {
		scrollObjectRigid = GetComponent<Rigidbody2D> ();
		scrollObjectRigid.velocity = new Vector2 (-1.25f, 0);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
