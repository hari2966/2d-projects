﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class GameOverMenu : MonoBehaviour
{

    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI highscoreText;
    public UIManager uIManager;
    // Start is called before the first frame update
    void OnEnable()
    {
        uIManager.OnGameOver += UIManager_OnGameOver;
    }

    

    private void OnDisable()
    {
        uIManager.OnGameOver -= UIManager_OnGameOver;
    }

    private void UIManager_OnGameOver()
    {
        GameManger.Instance.SaveHighScore();
        SetScore();
    } 

    // Update is called once per frame
    void Update()
    {
      

    }

    void SetScore()
    {        
        scoreText.text = GameManger.Instance.score.ToString();
        highscoreText.text = GameManger.Instance.HighScore.ToString();
    }
    

    public void Retry()
    {
        SceneManager.LoadScene(0);
    }

    public void Quit()
    {
        Application.Quit();
    }


}
