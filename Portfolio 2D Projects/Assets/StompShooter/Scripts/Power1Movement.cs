﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Power1Movement : MonoBehaviour {
	public float speed;
	public Rigidbody2D myrigid;

	public Ability ability_SO;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

		myrigid.velocity =  Vector2.left * speed;


	}


	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "Player") 
		{
			ability_SO.InitializeAbility(other.gameObject);
			Destroy (gameObject);


		}


	}
}
