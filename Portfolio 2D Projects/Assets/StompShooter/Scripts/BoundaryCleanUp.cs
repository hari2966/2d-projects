﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundaryCleanUp : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.GetComponent<IAttackable>() != null)
            GameManger.Instance.RemoveObject(other.gameObject);
        Destroy(other.gameObject);
    }
}
