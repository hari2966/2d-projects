﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName ="Spawn/PowerUp")]
public class PowerupSpawner : Spawner
{
    public GameObject[] spawnObjectList;
    public float startWaitTime;

}
