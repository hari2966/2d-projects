﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName ="Spawn/EnemySpawn")]
public class EnemySpawner : Spawner
{
    public GameObject spawnObject;   
    public int waveInterval;
    public int minSpawnCount;
    public int maxSpawnCount;
}
