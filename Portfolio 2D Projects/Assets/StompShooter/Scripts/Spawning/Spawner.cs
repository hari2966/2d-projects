﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class Spawner : ScriptableObject
{
    public int spawnInterval;
    public Vector2 minPos;
    public Vector2 maxPos;
}
