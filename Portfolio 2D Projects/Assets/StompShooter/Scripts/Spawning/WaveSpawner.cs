﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WaveSpawner : MonoBehaviour 
{
	
	public EnemySpawner[] enemySpawn_SO;
    public PowerupSpawner powerupSpawn_SO;

	// Use this for initialization

	private List <GameObject> enemiesSpawnedZ = new List <GameObject> ();	

	void OnEnable()
	{
        
	}

    

   

    void OnDisable()
	{
        EventBroker.OnEnemyDeath -= EventBroker_OnEnemyDeath;
    }



	void Start () {
        EventBroker.OnEnemyDeath += EventBroker_OnEnemyDeath;
        GameManger.Instance.OnGameStart += Instance_OnGameStart;
		
	}

    private void Instance_OnGameStart()
    {
        StartCoroutine(EnemySpawnWaves());
        StartCoroutine(PowerUpSpawn());
    }


    // Update is called once per frame
    void Update () {
	
	}

    private void EventBroker_OnEnemyDeath(GameObject obj)
    {
        GameManger.Instance.RemoveObject(obj);
    }




    IEnumerator EnemySpawnWaves()
    {
        while (true)
        {
            EnemySpawner spawnItem = enemySpawn_SO[Random.Range(0, enemySpawn_SO.Length)];

            int spawnCount = Random.Range(spawnItem.minSpawnCount, spawnItem.maxSpawnCount);

            for (int i = 0; i < spawnCount; i++)
            {
                float xVal = Random.Range(spawnItem.minPos.x, spawnItem.maxPos.x);
                float yVal = Random.Range(spawnItem.minPos.y, spawnItem.maxPos.y);

                Vector3 SpawnPosition = new Vector3(xVal, yVal, 0);
                Quaternion SpawnRotation = Quaternion.identity;
                GameObject enemyClone = Instantiate(spawnItem.spawnObject, SpawnPosition, SpawnRotation) as GameObject;
                GameManger.Instance.AddObjects(enemyClone);
                yield return new WaitForSeconds(spawnItem.spawnInterval);
            }
            yield return new WaitForSeconds(spawnItem.waveInterval);



        }
    }

    IEnumerator PowerUpSpawn()
    {
        while (true)
        {
            GameObject powerUpObject = powerupSpawn_SO.spawnObjectList[Random.Range(0, powerupSpawn_SO.spawnObjectList.Length)];

            yield return new WaitForSeconds(powerupSpawn_SO.startWaitTime);
            
            float xVal = Random.Range(powerupSpawn_SO.minPos.x, powerupSpawn_SO.maxPos.x);
            float yVal = Random.Range(powerupSpawn_SO.minPos.y, powerupSpawn_SO.maxPos.y);

            Vector3 SpawnPosition = new Vector3(xVal, yVal, 0);
            Quaternion SpawnRotation = Quaternion.identity;
            GameObject enemyClone = Instantiate(powerUpObject, SpawnPosition, SpawnRotation) as GameObject;
            yield return new WaitForSeconds(powerupSpawn_SO.spawnInterval);
           
        }



    }

}


