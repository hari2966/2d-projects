﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldColliision : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Enemy"))
        {
            IAttackable enemy = collision.gameObject.GetComponent<IAttackable>();
            enemy.OnAttack(100);
            gameObject.SetActive(false);           
        }

        if(collision.gameObject.CompareTag("EnemyBullet"))
        {
            EnemtBulletMovement bullet = collision.gameObject.GetComponent<EnemtBulletMovement>();
            Destroy(bullet.gameObject);
            gameObject.SetActive(false);
        }
    }
}
